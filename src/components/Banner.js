import { Button, Row, Col, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

export default function Banner() {
	return (
		<Row>
			<Col>
				<h5>Zuitt Coding Bootstrap</h5>
				<h1>Page Not Found</h1>
				<Nav>
        		 Go back to the<Nav.Link as={NavLink} to="/">HomePage</Nav.Link>
      			</Nav>
			</Col>
		</Row>
	);
}