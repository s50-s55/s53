import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

import { useNavigate } from 'react-router-dom';

export default function Login(props){

	// hook return a function that lets you navigate to components.
	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState("true")

	function authenticate(e) {

		e.preventDefault();

		// Set the email of the authenticated user in the local storage
			// Syntax
			// localStorage.setItem('propertyName', value)
		localStorage.setItem("email", email);

		// In this case, the key name is 'email', and the value is the value of the email varaible. This code sets the value of 'email' key in the local storege to the value of the email varaible

		setEmail("");
		setPassword("");
		navigate('/');

		alert(`"${email}" has been verified! Welcome back!`);
	}

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password]);

	return (
		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			{ isActive ? 

				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>

				: 

				<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>

			}
			
		</Form>
	)
}